import React from 'react';
import './ProductList.css';

export class PoduList extends React.Component {
    constructor(props){
        super(props);
        
    }
    
    render(){
        let products = this.props.source.map((product, index) =>{
            return <div key={index}>{product.productName}</div>
        })
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div className="products-container">
                {products}
                </div>
            </div>
            );
    }
}