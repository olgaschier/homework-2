import React from 'react';
import axios from 'axios';

export class AddProduct extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            productName: "",
            price: 0,
        };
    }
    
    clearFields = () => {
        this.setState({
           productName: "",
           price: 0,
        })
    }
    
    handleChangeProductName = (e) => {
        this.setState({
           productName: e.target.value
        });
        
    }
    
    handleChangePrice = (e) => {
        this.setState({
            price: e.target.value
        });
        
    }
    
    onItemAdded = () => {
      let product = {
          productName: this.state.productName,
          price: this.state.price
      }
      this.props.handleAdd(product);
      this.clearFields()
    }
    
    render(){
        return (
            <React.Fragment>
                <div>
                    <h1>Add Product </h1>
                    <div>
                        <input type="text" placeholder="Product Name" value={this.state.productName} onChange={this.handleChangeProductName} />
                        <input type="number" value={this.state.price}  onChange={this.handleChangePrice}/>
                        <button onClick={this.onItemAdded}>Add Product</button>
                    </div>
                </div>
            </React.Fragment>
            )
    }
}